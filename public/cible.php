<?php
try
{
// On se connecte à MySQL

$bdd = new PDO('mysql:host=localhost;dbname=GIP_schema;charset=utf8', 'root', 'userpop');

}
catch(Exception $e)
{

// En cas d'erreur, on affiche un message et on arrête tout
die('Erreur : '.$e->getMessage());
}

$req = $bdd->prepare("INSERT INTO users (email, password, pseudo)
VALUES (?,?,?)");
$id = $bdd->prepare("SELECT COUNT(*) FROM users WHERE email = ? OR pseudo = ?");
$id->execute(array($_POST['email'], $_POST['pseudo']));

if ($id->fetchColumn() != 0) {
  echo "<script type='text/javascript'>alert('Tu as déjà un compte');</script>";
} else {
  $req->execute(array($_POST['email'], password_hash($_POST['passwordOne'], PASSWORD_BCRYPT), $_POST['pseudo']));
  $message = "Bravo ! Ton compte a été créé";
  echo "<script type='text/javascript'>alert('$message');</script>";
}

$req->closeCursor();
$id->closeCursor();

 ?>
